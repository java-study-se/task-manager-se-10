package com.morozov.tm.command.data.xml;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;

public class DataXmlJaxBSaveCommand extends AbstractCommand {
    public DataXmlJaxBSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Save data to xml file with Jax-B library";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        ConsoleHelperUtil.writeString("Сохраняем в xml файл с помощью Jax-B");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_XML.getPath());
        Files.createDirectory(pathToFile.getParent());
        final File sourceFile = pathToFile.toFile();
        final FileOutputStream fileOutputStream = new FileOutputStream(sourceFile);
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(domain,sourceFile);
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
