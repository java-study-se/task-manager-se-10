package com.morozov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.ConsoleHelperUtil;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;

public class DataXmlJacksonLoadCommand extends AbstractCommand {
    public DataXmlJacksonLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-xml-jackson";
    }

    @Override
    public String getDescription() {
        return "Load data from xml file with Jax-B library";
    }

    @Override
    public void execute() throws IOException {
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_XML.getPath());
        final Path pathToFile = Paths.get(DataConstant.DATA_FILE_XML.getPath());
        final File sourceFile = pathToFile.toFile();
        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = objectMapper.readValue(sourceFile,Domain.class);
        serviceLocator.getDomainService().load(domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
