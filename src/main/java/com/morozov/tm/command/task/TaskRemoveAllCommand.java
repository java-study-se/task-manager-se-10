package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.Nullable;

public class TaskRemoveAllCommand extends AbstractCommand {
    public TaskRemoveAllCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @Override
    public String getName() {
        return "task-removeall";
    }

    @Override
    public String getDescription() {
        return "Remove all task current user";
    }

    @Override
    public void execute(){
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if(currentUser != null){
            ConsoleHelperUtil.writeString("Удаление задач пользователя " + currentUser.getName());
            serviceLocator.getTaskService().removeAllTaskByUserId(currentUser.getId());
            ConsoleHelperUtil.writeString("Удаление задач завершено");
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
