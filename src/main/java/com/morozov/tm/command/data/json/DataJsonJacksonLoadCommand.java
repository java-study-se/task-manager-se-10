package com.morozov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;

public class DataJsonJacksonLoadCommand extends AbstractCommand {
    public DataJsonJacksonLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-json-jackson";
    }

    @Override
    public String getDescription() {
        return "Load data from json file with Jax-B library";
    }

    @Override
    public void execute() throws IOException {
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_JSON.getPath());
        @Nullable final Path pathToFile = Paths.get(DataConstant.DATA_FILE_JSON.getPath());
        @Nullable File sourceFile;
        if (pathToFile == null) return;
        sourceFile = pathToFile.toFile();
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readValue(sourceFile, Domain.class);
        if (domain == null) return;
        serviceLocator.getDomainService().load(domain);
        if (sourceFile == null) return;
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
