package com.morozov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;

public class DataJsonJacksonSaveCommand extends AbstractCommand {
    public DataJsonJacksonSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-json-jackson";
    }

    @Override
    public String getDescription() {
        return "Save data to json file with Jackson library";
    }

    @Override
    public void execute() throws IOException {
        ConsoleHelperUtil.writeString("Сохраняем в json файл с помощью Jax-B");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_JSON.getPath());
        Files.createDirectory(pathToFile.getParent());
        final File sourceFile = pathToFile.toFile();
        final ObjectMapper objectMapper = new ObjectMapper();
        final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(sourceFile,domain);
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
