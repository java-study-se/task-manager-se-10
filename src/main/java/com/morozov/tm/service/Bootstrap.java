package com.morozov.tm.service;

import com.morozov.tm.api.*;
import com.morozov.tm.command.*;
import com.morozov.tm.exception.*;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.entity.User;
import com.morozov.tm.repository.UserRepository;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.*;

public final class Bootstrap implements IServiceLocator {
    @NotNull
    final private IProjectService projectService = new ProjectService(new ProjectRepository());
    @NotNull
    final private ITaskService taskService = new TaskService(new TaskRepository());
    @NotNull
    final private IUserService userService = new UserService(new UserRepository());
    @NotNull
    final private Map<String, AbstractCommand> commands = new TreeMap<>();
    @NotNull
    final private IDomainService domainService = new DomainService(this);
    @Nullable
    private User currentUser = null;

    public void init(@NotNull Set<Class<? extends AbstractCommand>> classes) {
        ConsoleHelperUtil.writeString("Вас приветствует программа Task Manager. " +
                "Наберите \"help\" для вывода списка доступных команд. ");
        @NotNull String console = "";
        while (!"exit".equals(console)) {
            commands.clear();
            try {
                fillCommands(classes);
                console = ConsoleHelperUtil.readString();
                AbstractCommand command = commands.get(console);
                if (command != null) {
                    command.execute();
                }
            } catch (UserExistException | UserNotFoundException | CommandCorruptException | ParseException |
                    RepositoryEmptyException | StringEmptyException | TaskNotFoundException | ProjectNotFoundException e) {
                ConsoleHelperUtil.writeString(e.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@Nullable User user) {
        currentUser = user;
    }

    @Override
    public @NotNull IDomainService getDomainService() {
        return domainService;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    private void registry(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void fillCommands(@NotNull Set<Class<? extends AbstractCommand>> classes) throws CommandCorruptException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        for (Class clazz : classes) {
            @NotNull AbstractCommand command = (AbstractCommand) Class.forName(clazz.getName()).newInstance();
            if (command != null) {
                registryAllUserCommand(command);
                if (currentUser != null) {
                    registryUserSessionCommand(command);
                } else {
                    registryEmptyUserSessionCommand(command);
                }
            }
        }
    }

    private void registryAllUserCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (command.getUserRoleList().contains(UserRoleEnum.ALL)) registry(command);
    }

    private void registryEmptyUserSessionCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (command.getUserRoleList().isEmpty()) registry(command);
    }

    private void registryUserSessionCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        assert currentUser != null;
        if (command.getUserRoleList().contains(currentUser.getRole())) registry(command);
    }

}

