package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ComparatorFactoryUtil;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.enumerated.CompareTypeUnum;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractCommand {
    public ProjectListCommand() {
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all projects";
    }

    @Override
    final public void execute() throws RepositoryEmptyException {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if (currentUser != null) {
            @NotNull final List<Project> projectList = serviceLocator.getProjectService().getAllProjectByUserId(currentUser.getId());
            ConsoleHelperUtil.writeString("Список проектов пользователя " + currentUser.getName() + " :");
            @NotNull final CompareTypeUnum compareTypeUnum = ConsoleHelperUtil.printSortedVariant();
            if (compareTypeUnum != null) {
                ConsoleHelperUtil.writeString("Сортировка по " + compareTypeUnum.getName());
                @NotNull final Project[] projects = projectList.toArray(new Project[0]);
                Arrays.sort(projects, ComparatorFactoryUtil.getComparator(compareTypeUnum));
                ConsoleHelperUtil.printProjectList(Arrays.asList(projects));
            } else {
                ConsoleHelperUtil.writeString("Сортировка не выбрана.");
                ConsoleHelperUtil.printProjectList(projectList);
            }

        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
