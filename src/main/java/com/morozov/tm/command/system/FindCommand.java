package com.morozov.tm.command.system;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class FindCommand extends AbstractCommand {
    public FindCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @Override
    public String getName() {
        return "find";
    }

    @Override
    public String getDescription() {
        return "Find project and task by name or description";
    }

    @Override
    public void execute() throws RepositoryEmptyException {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();

        if (currentUser != null) {
            ConsoleHelperUtil.writeString("Введите строку для поиска");
            @NotNull final String findString = ConsoleHelperUtil.readString().trim();
            @NotNull final String currentUserId = currentUser.getId();
            @NotNull final List<Project> projectsList = serviceLocator
                    .getProjectService()
                    .findProjectByStringInNameOrDescription(currentUserId, findString);
            @NotNull final List<Task> taskList = serviceLocator
                    .getTaskService()
                    .findTaskByStringInNameOrDescription(currentUserId, findString);
            if (findString.isEmpty()) {
                ConsoleHelperUtil.writeString("Введенная строка пуста, будет выведен полный список проектов и " +
                        "задач пользователя " + currentUser.getName());
            }
            ConsoleHelperUtil.printProjectList(projectsList);
            ConsoleHelperUtil.printTaskList(taskList);
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
