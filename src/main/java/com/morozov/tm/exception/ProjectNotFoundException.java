package com.morozov.tm.exception;

public class ProjectNotFoundException extends Exception {
    public ProjectNotFoundException() {
        super("Проект не найден");
    }
}
