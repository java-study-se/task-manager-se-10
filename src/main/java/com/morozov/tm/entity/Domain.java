package com.morozov.tm.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
@XmlRootElement
public final class Domain implements Serializable {
    @Nullable
    private List<Project> projectList = new ArrayList<>();
    @Nullable
    private List<Task> taskList = new ArrayList<>();
    @Nullable
    private List<User> userList = new ArrayList<>();
}
