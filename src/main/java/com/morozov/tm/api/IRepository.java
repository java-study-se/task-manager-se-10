package com.morozov.tm.api;

import com.morozov.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T extends AbstractEntity> {
    @NotNull
    List<T> findAll();
    @Nullable
    T findOne(@NotNull final String id);

    void persist(@NotNull final String id, @NotNull final T writeEntity);

    void merge(@NotNull final String id,@NotNull final T updateEntity);

    void remove(@NotNull final String id);

    void removeAll();
    void load(@NotNull final List<T> loadEntityList);
}
