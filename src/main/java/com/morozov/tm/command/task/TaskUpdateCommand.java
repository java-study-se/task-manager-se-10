package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.TaskNotFoundException;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;

public class TaskUpdateCommand extends AbstractCommand {

    public TaskUpdateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update selected task";
    }

    @Override
    final public void execute() throws ParseException, RepositoryEmptyException, StringEmptyException, TaskNotFoundException {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите ID задачи для изменения");
        @NotNull final String updateTaskID = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое имя задачи");
        @NotNull final String updateTaskName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое описание задачи");
        @NotNull final String updateTaskDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату начала задачи в формате DD.MM.YYYY");
        @NotNull final String startUpdateTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату окончания задачи в формате DD.MM.YYYY");
        @NotNull final String endUpdateTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите ID проекта задачи");
        @NotNull final String updateTaskProjectId = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            serviceLocator.getTaskService().updateTask(currentUser.getId(), updateTaskID, updateTaskName,
                    updateTaskDescription, startUpdateTaskDate, endUpdateTaskDate, updateTaskProjectId);
            ConsoleHelperUtil.writeString("Задача " + updateTaskName + " обновлена");
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
