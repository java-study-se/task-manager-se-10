package com.morozov.tm.api;

import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.TaskNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    @NotNull
    List<Task> findAllTask();
    @NotNull
    List<Task> getAllTaskByUserId(@NotNull String userId) throws RepositoryEmptyException;
    @NotNull
    Task addTask(@NotNull String userId, @NotNull String taskName, @NotNull String projectId)
            throws StringEmptyException;

    boolean removeTaskById(@NotNull String userId, @NotNull String id) throws StringEmptyException;

    void updateTask(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description,
            @NotNull String dataStart, @NotNull String dataEnd, @NotNull String projectId)
            throws RepositoryEmptyException, StringEmptyException, ParseException, TaskNotFoundException;
    @NotNull
    List<Task> getAllTaskByProjectId(@NotNull String userId, @NotNull String projectId)
            throws StringEmptyException, RepositoryEmptyException;

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    void clearTaskList();
    @NotNull
    List<Task> findTaskByStringInNameOrDescription(@NotNull String userId, @NotNull String string)
            throws RepositoryEmptyException;

    void removeAllTaskByUserId(@NotNull String userId);

    void loadTaskList(@Nullable final List<Task> loadList);
}
