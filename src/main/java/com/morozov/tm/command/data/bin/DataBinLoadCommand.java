package com.morozov.tm.command.data.bin;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataBinLoadCommand extends AbstractCommand {
    public DataBinLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-bin";
    }

    @Override
    public String getDescription() {
        return "Load data from binnary file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_BIN.getPath());
        @Nullable final Path pathToFile = Paths.get(DataConstant.DATA_FILE_BIN.getPath());
        @Nullable File sourceFile;
        if (pathToFile == null) return;
        sourceFile = pathToFile.toFile();
        @Nullable FileInputStream fileInputStream;
        if (sourceFile == null) return;
        fileInputStream = new FileInputStream(sourceFile);
        @Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        if (domain == null) return;
        serviceLocator.getDomainService().load(domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
