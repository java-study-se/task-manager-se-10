package com.morozov.tm.command.data.json;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class DataJsonJaxBSaveCommand extends AbstractCommand {
    public DataJsonJaxBSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-json-jaxb";
    }

    @Override
    public String getDescription() {
        return "Save data to json file with Jax-B library";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        ConsoleHelperUtil.writeString("Сохраняем в json файл с помощью Jax-B");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_JSON.getPath());
        Files.createDirectory(pathToFile.getParent());
        final File sourceFile = pathToFile.toFile();
        final FileOutputStream fileOutputStream = new FileOutputStream(sourceFile);
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final Map<String,Object> properties = new HashMap<>();
        properties.put(JAXBContextProperties.MEDIA_TYPE,"application/json");
        final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] {Domain.class}, properties);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(domain,fileOutputStream);
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
