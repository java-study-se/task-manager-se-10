package com.morozov.tm.command.data.bin;


import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataBinSaveCommand extends AbstractCommand {
    public DataBinSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-bin";
    }

    @Override
    public String getDescription() {
        return "Save data to binary file";
    }

    @Override
    public void execute() throws IOException {
        ConsoleHelperUtil.writeString("Сохраняем в bin файл");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        @Nullable final Path pathToFile = Paths.get(DataConstant.DATA_FILE_BIN.getPath());
        if (pathToFile == null) return;
        Files.createDirectory(pathToFile.getParent());
        @Nullable File sourceFile;
        sourceFile = pathToFile.toFile();
        @Nullable FileOutputStream fileOutputStream;
        if (sourceFile == null) return;
        fileOutputStream = new FileOutputStream(sourceFile);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
