package com.morozov.tm.command.data.xml;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.ConsoleHelperUtil;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;

public class DataXmlJaxBLoadCommand extends AbstractCommand {
    public DataXmlJaxBLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Load data from xml file with Jax-B library";
    }

    @Override
    public void execute() throws JAXBException {
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_XML.getPath());
        final Path pathToFile = Paths.get(DataConstant.DATA_FILE_XML.getPath());
        final File sourceFile = pathToFile.toFile();
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(sourceFile);
        serviceLocator.getDomainService().load(domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
