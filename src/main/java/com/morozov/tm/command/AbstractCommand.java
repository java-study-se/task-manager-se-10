package com.morozov.tm.command;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.exception.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;
    protected List<UserRoleEnum> userRoleList = new ArrayList<>();

    public void setServiceLocator(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws ParseException, StringEmptyException, RepositoryEmptyException, UserNotFoundException, UserExistException, IOException, ClassNotFoundException, JAXBException, TaskNotFoundException, ProjectNotFoundException;

    public List<UserRoleEnum> getUserRoleList() {
        return userRoleList;
    }
}
