package com.morozov.tm.api;

import com.morozov.tm.entity.Domain;
import org.jetbrains.annotations.NotNull;

public interface IDomainService {
    void load(@NotNull Domain domain);
    void export(@NotNull Domain domain);
}
