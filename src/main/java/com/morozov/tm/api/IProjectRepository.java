package com.morozov.tm.api;


import com.morozov.tm.entity.AbstractEntity;
import com.morozov.tm.entity.AbstractWorkEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository<T extends AbstractWorkEntity> extends IRepository<T> {
    @NotNull
    List<T> findAllByUserId(@NotNull final String userId);

    @Nullable
    T findOneByUserId(@NotNull final String userId, @NotNull final String id);
    @NotNull
    List<T> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string);

    void removeAllByUserId(@NotNull final String userId);
}
