package com.morozov.tm.exception;

public class CommandCorruptException extends Exception {
    public CommandCorruptException() {
        super("Команда не найдена");
    }
}
