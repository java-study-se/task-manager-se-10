package com.morozov.tm.command.data.json;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.eclipse.persistence.jaxb.JAXBContextProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class DataJsonJaxBLoadCommand extends AbstractCommand {
    public DataJsonJaxBLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-json-jaxb";
    }

    @Override
    public String getDescription() {
        return "Load data from json file with Jax-B library";
    }

    @Override
    public void execute() throws FileNotFoundException, JAXBException {
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_JSON.getPath());
        final Path pathToFile = Paths.get(DataConstant.DATA_FILE_JSON.getPath());
        final File sourceFile = pathToFile.toFile();
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final Map<String,Object> properties = new HashMap<>();
        properties.put(JAXBContextProperties.MEDIA_TYPE,"application/json");
        final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] {Domain.class}, properties);
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        final Domain domain = (Domain) unmarshaller.unmarshal(sourceFile);
        serviceLocator.getDomainService().load(domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
